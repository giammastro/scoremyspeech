//
//  ViewController.swift
//  testProject
//
//  Created by Giammarco Mastronardi on 07/11/2019.
//  Copyright © 2019 Giammarco Mastronardi. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    var sadPlayer = AVAudioPlayer()
    var serenePlayer = AVAudioPlayer()
    var interrogativePlayer = AVAudioPlayer()
    
    @IBOutlet weak var greetingsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let sadMusic = Bundle.main.path(forResource: "ASD_negative_sad", ofType: "mp3")
        // copy this syntax, it tells the compiler what to do when action is received
        do {
            sadPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sadMusic! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
      
        super.viewDidLoad()
        let sereneMusic = Bundle.main.path(forResource: "ASD_positive_happy_serene", ofType: "mp3")
        do {
            serenePlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sereneMusic! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
        
        super.viewDidLoad()
        let interrogativeMusic = Bundle.main.path(forResource: "ASD_interrogative", ofType: "mp3")
        do {
            interrogativePlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: interrogativeMusic! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            }
        catch{
            print(error)
        }
    }
        
    
    
    @IBAction func sereneButton(_ sender: Any) {
        serenePlayer.play()
    }
    
    @IBAction func sadButton(_ sender: Any) {
        sadPlayer.play()
    }
    
    @IBAction func interrogativePlayer(_ sender: Any) {
        interrogativePlayer.play()
    }
    
    
//    @IBAction func buttonAction(_ sender: Any) {
//        audioPlayer.play()
//        greetingsLabel.text = "I WILL HAUNT YOUR DREAMS FOREVER"
//    }
//
//    @IBAction func royaltyButton(_ sender: Any) {
//        audioPlayer2.play()
//
//    }
}

